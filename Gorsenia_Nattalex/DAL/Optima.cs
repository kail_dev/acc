﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using CDNBase;
using CDNLib;
using CDNTwrb1;
using NLog;
namespace Nattalex.DAL
{
    public class optima
    {
        public static Logger logger = LogManager.GetCurrentClassLogger();


        protected static Application Application = null;
        protected static ILogin Login = null;
        public bool LogowanieAutomatyczne()
        {
            string Operator = "Admin";
            string Haslo = "";
            string Firma = "demo1";

            object[] hPar = new object[]
            {
                0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0
            }; // do jakich modułów będzie logowanie
            /* Kolejno: KP, KH, KHP, ST, FA, MAG, PK, PKXL, CRM, ANL, DET, BIU, SRW, ODB, KB, KBP, HAP  
             */

            // katalog, gdzie jest zainstalowana Optima (bez ustawienia tej zmiennej nie zadziała, chyba że program odpalimy z katalogu O!)
            System.Environment.CurrentDirectory = @"C:\Program Files (x86)\Comarch ERP Optima";

            // tworzenie obiektu apliakcji
            Application = new Application();
            // blokowanie
            Application.LockApp(513, 5000, null, null, null, null);
            // logowanie do podanej Firmy, na danego operatora, do podanych modułów

            try
            {
                Console.WriteLine("Logowanie...");
                Login = Application.Login(Operator, Haslo, Firma, hPar[0], hPar[1], hPar[2], hPar[3], hPar[4], hPar[5],
                    hPar[6], hPar[7], hPar[8], hPar[9], hPar[10], hPar[11], hPar[12], hPar[13], hPar[14], hPar[15],
                    hPar[16]);
                Console.WriteLine("Prawidowe zalogowanie");
                return true;
            }
            catch
            {
                Console.WriteLine("Logowanie zakończono niepowodzeniem.");
                return false;
            }

            // tu jestemy zalogowani do O!            
        }

        public bool Payments(string data, string kwota)
        {
            try
            {
                CDNBase.AdoSession session = Login.CreateSession();


                var register = (OP_KASBOLib.Rachunek)session.CreateObject("CDN.Rachunki")
                            .Item("BRa_Symbol = 'KASA'");

                var kp = (CDNHeal.DefinicjaDokumentu)session.CreateObject("CDN.DefinicjeDokumentow")
                    .Item("DDf_Symbol='KP'");
                var record = session.CreateObject("CDN.ZapisyKB").AddNew();
                var stronglyTypedRecord = record as OP_KASBOLib.ZapisKB;

                if (stronglyTypedRecord != null)
                {

                    stronglyTypedRecord.Rachunek = register;
                    stronglyTypedRecord.DataDok = Convert.ToDateTime(data);
                    stronglyTypedRecord.DefinicjaDokumentu = kp;
                    stronglyTypedRecord.Kwota = Convert.ToDecimal(kwota);

                    session.Save();

                }
                return true;
            }
            catch (COMException)
            {
                return false;
            }


        }
     
    
    

    public void Rabaty()
        {
            CDNBase.AdoSession Sesja = Login.CreateSession();
          
            var rabaty = (CDNTwrb1.Rabaty)Sesja.CreateObject("CDN.Rabaty", null);
            var rabat = (CDNTwrb1.IRabat)rabaty.AddNew();
            rabat.Cena = 1111;
            rabat.Typ = 6;
            rabat.TowarID = 26;
            rabat.PodmiotTyp = 1;
            rabat.PodmiotID = 3;



                
            Sesja.Save();
       //     Console.ReadKey();
      // 
     //  
    //
   //
        }

        public CDNKH.IDekret PK(AdoSession session , string konto="",string dokument ="",string kwotama="", string kwotawn="")

        {

          string   kontr = konto.Substring(konto.IndexOf("-")+1, konto.Length - konto.IndexOf("-")-1);
                var dekret = (CDNKH.Dekrety)session.CreateObject("CDN.Dekrety", null);
              var idekret = (CDNKH.IDekret)dekret.AddNew(null);
            var dziennik = (CDNBase.ICollection)(session.CreateObject("CDN.Dzienniki", null));
            //CDNDave.IDziennik idzienniki = (CDNDave.IDziennik)dziennik["Dzi_DziID = 26"];
            var idzienniki = (CDNDave.IDziennik)dziennik["Dzi_Symbol = 'BANK' AND Dzi_OObID='1'"];
            idekret.DziID = idzienniki.ID;
            idekret.DataDok = new DateTime(2020, 06, 9); //data

            var Kontrahenci = (CDNBase.ICollection)(session.CreateObject("CDN.Kontrahenci", null));
            var kontrahent = (CDNHeal.IKontrahent)Kontrahenci[$"Knt_Kod='{kontr}'"];

         
            idekret.Bufor = 1;
            idekret.Podmiot = (CDNHeal.IPodmiot)kontrahent;
            idekret.Dokument = dokument;
         /*   ICollection elementy = idekret.Elementy;
            var ielement = (CDNKH.IDekretElement)elementy.AddNew(null);

            if (kwotama == "0")
            {
                var konta = (ICollection)session.CreateObject("CDN.Konta", null);

                var kontoWn = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];
                ielement.KontoWn = (CDNKH.Konto)kontoWn;

                ielement.Kwota = Convert.ToDecimal(kwotawn);
                //  var kontoMa = (CDNKH.IKonto)konta[$"Acc_Numer = '131'"];
            }
            else
            {
                var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                var kontoMA = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];

                ielement.KontoMa = (CDNKH.Konto)kontoMA;

                ielement.Kwota = Convert.ToDecimal(kwotama);
            }*/
      
            return idekret;
            
        }

        public void PKposition(AdoSession session,string konto , string data, string kwotawn, string kwotama, CDNKH.IDekret idekret, string dokument)
        {
            string account = "";
            string czlon2 = konto.Substring(konto.IndexOf("-") + 1, konto.Length - konto.IndexOf("-") - 1);

            //var counter = 1;
            var counter = konto.Count(x => x == '-');
            
            if (konto.Length > 3)
            {
                account = konto.Substring(0, konto.IndexOf("-"));

            }
            else
            {
                account = konto;
            }


            if(counter ==1 && account=="200" || account=="202"  )
            {

               try
                { 
                // Dodawania pozycji do dokumentu
                ICollection elementy = idekret.Elementy;
                var ielement = (CDNKH.IDekretElement)elementy.AddNew(null);

                if (kwotama=="0")
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);

                    var kontoWn = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];
                      ielement.KontoWn = (CDNKH.Konto)kontoWn;

                    ielement.Kwota =Convert.ToDecimal(kwotawn);
                    //  var kontoMa = (CDNKH.IKonto)konta[$"Acc_Numer = '131'"];
                }
                else
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var kontoMA = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];
                  
                    ielement.KontoMa = (CDNKH.Konto)kontoMA;

                    ielement.Kwota = Convert.ToDecimal(kwotama);
                }

                }
                catch(Exception e)
                {
                    Console.WriteLine(e);
                }






            }


            //  202 - !NIEOKREŚLONY!
            //   string account =konto.Substring(0,)


            if (counter == 1 && account != "202" || account != "202")
            {
                try
                {

                
                ICollection elementy = idekret.Elementy;
                var ielement = (CDNKH.IDekretElement)elementy.AddNew(null);

                if (kwotama == "0")
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);

                    var kontoWn = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];
                    ielement.KontoWn = (CDNKH.Konto)kontoWn;

                    ielement.Kwota = Convert.ToDecimal(kwotawn);
                    //  var kontoMa = (CDNKH.IKonto)konta[$"Acc_Numer = '131'"];
                }
                else
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var kontoMA = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];

                    ielement.KontoMa = (CDNKH.Konto)kontoMA;

                    ielement.Kwota = Convert.ToDecimal(kwotama);
                }
                }
                catch(Exception e)
                {
                    AddAcc(konto, konto);
                }


            }



            Console.WriteLine("{0} {1} {2}", konto,kwotama,kwotawn);
         
        }





        public void PKpositionADD(AdoSession session, string konto, string data, string kwotawn, string kwotama,int giddek)
        {
            string account = "";
            string czlon2 = konto.Substring(konto.IndexOf("-") + 1, konto.Length - konto.IndexOf("-") - 1);

            var dekret = (CDNKH.Dekrety)session.CreateObject("CDN.Dekrety", null);
            var idekret = (CDNKH.IDekret)dekret[$"DeN_DeNId='{giddek}'"];
            var counter = 1;
            konto.Count(x => x == '-');

            if (konto.Length > 3)
            {
                account = konto.Substring(0, konto.IndexOf("-"));

            }
            else
            {
                account = "10";
            }


            if (counter == 1 && account == "200" || account == "202")
            {


                // Dodawania pozycji do dokumentu
                ICollection elementy = idekret.Elementy;
                var ielement = (CDNKH.IDekretElement)elementy.AddNew(null);

                if (kwotama == "0")
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);

                    var kontoWn = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];
                    ielement.KontoWn = (CDNKH.Konto)kontoWn;

                    ielement.Kwota = Convert.ToDecimal(kwotawn);
                    //  var kontoMa = (CDNKH.IKonto)konta[$"Acc_Numer = '131'"];
                }
                else
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var kontoMA = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];

                    ielement.KontoMa = (CDNKH.Konto)kontoMA;

                    ielement.Kwota = Convert.ToDecimal(kwotama);
                }








            }


            //  202 - !NIEOKREŚLONY!
            //   string account =konto.Substring(0,)


            if (counter == 1 && account != "202" || account != "202")
            {

                ICollection elementy = idekret.Elementy;
                var ielement = (CDNKH.IDekretElement)elementy.AddNew(null);

                if (kwotama == "0")
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);

                    var kontoWn = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];
                    ielement.KontoWn = (CDNKH.Konto)kontoWn;

                    ielement.Kwota = Convert.ToDecimal(kwotawn);
                    //  var kontoMa = (CDNKH.IKonto)konta[$"Acc_Numer = '131'"];
                }
                else
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var kontoMA = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];

                    ielement.KontoMa = (CDNKH.Konto)kontoMA;

                    ielement.Kwota = Convert.ToDecimal(kwotama);
                }



            }





        }


        public CDNKH.Bilans BOCreate(AdoSession session)
        {
            
          

            
            var rr = (CDNKH.Bilans)session.CreateObject("CDN.Bilansy").AddNew();
           
                return rr;
            }
           
           // return true;
           
        
        public void BOPosition(string konto,string kwotawn,string kwotama, CDNKH.Bilans rr, AdoSession session)
        {
            try
            {
                var rKonto = session.CreateObject("CDN.Konta").Item($"acc_numer = '{konto}'");

                var rElement =rr.Elementy.AddNew();
                rElement.Konto = rKonto;
                rElement.KwotaWn = kwotawn;

                rElement.KwotaMa = kwotama;

            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                //return false;
            }
            }





        public void AddAcc(string NrKonta, string nazwa)
        {
        
            CDNBase.AdoSession session = Login.CreateSession();
            var counter = 0;
            try {
                counter = NrKonta.Count(x => x == '-');
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            string account = "";
            if (NrKonta.Length > 3)
            {
                account = NrKonta.Substring(0, 3);

            }
            else
            {
                account = NrKonta;
            }
            if (counter == 0 && account != "200" && account != "202" && account != "205")
            {
                try
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var konto = (CDNKH.IKonto)konta.AddNew(null);


                    konto.Nazwa = nazwa;
                    konto.TypKonta = 1;
                   
                    konto.Analityka = 0;
                
                    konto.Segment = NrKonta;
                    konto.OkresId = 1;
       
                    session.Save();

                }
                catch (Exception e)
                {
                    Console.WriteLine("Bledy konta proste {0}", NrKonta);
                    Console.WriteLine(e);
                }

            }
            if (counter == 1 && account != "200" && account != "202" && account != "205")

            {
                try
                {


                    string czlon6 = NrKonta.Substring(0, NrKonta.IndexOf("-"));
                    int test = (NrKonta.IndexOf("-"));
                    string czlon7 = NrKonta.Substring(NrKonta.IndexOf("-") + 1, NrKonta.Length - NrKonta.IndexOf("-") - 1);
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var konto = (CDNKH.IKonto)konta.AddNew(null);
                    //   string rodzicid = czlon2.Substring(1, 2);
                    var rodzicKonto = (CDNKH.IKonto)konta[$"ACC_NumerIdx ='{czlon6}'"];

                    //  konto.NumerIdx = "10";
                    konto.Nazwa = nazwa;
                    konto.ParId = rodzicKonto.ID;
                    konto.Analityka = 1;
                    konto.TypKonta = 1;
                    konto.Segment = czlon7;
                    konto.OkresId = 1;
                    konto.Poziom = 2;
                    session.Save();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Bledy 1 {0}", NrKonta);
                    Console.WriteLine(e);
                }
            }
            if (counter == 2 && account != "200" && account != "202" && account != "205")
            {
                try
                {


                    string czlon10 = NrKonta.Substring(0, NrKonta.IndexOf("-"));

                    int test = (NrKonta.IndexOf("-"));
                    string czlon2full = NrKonta.Substring(NrKonta.IndexOf("-") + 1, NrKonta.Length - NrKonta.IndexOf("-") - 1);

                    string czlon12 = czlon2full.Substring(0, czlon2full.IndexOf("-"));



                    string czlon3 = czlon2full.Substring(czlon2full.IndexOf("-") + 1, czlon2full.Length - czlon2full.IndexOf("-") - 1);



                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var konto = (CDNKH.IKonto)konta.AddNew(null);
                    string kontor = czlon10 + "." + czlon12;
                    // var rodzicKonto = (CDNKH.IKonto)konta[$"Acc_Numer ={ kontor} "];
                    var rodzickonto = (CDNKH.IKonto)konta[$"Acc_NumerIdx ='{kontor}'"];

                    konto.NumerIdx = NrKonta;
                    konto.Nazwa = NrKonta;
                    konto.ParId = rodzickonto.ID;
                    konto.Segment = czlon12;
                    konto.OkresId = 1;
                    konto.Poziom = 3;
                    session.Save();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Bledy 2 {0}", NrKonta);
                    Console.WriteLine(e);
                }
            }






            if (counter == 0 && account == "205" )
            {
                try
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var konto = (CDNKH.IKonto)konta.AddNew(null);


                    //konto.NumerIdx = NrKonta;
                    konto.Nazwa = nazwa;
                    konto.TypKonta = 1;
                    //konto.Poziom = 1;
                    konto.Analityka = 0;
                    //konto.NextAcc = NrKonta;
                    konto.Segment = NrKonta;
                    konto.OkresId = 1;
                    //  konto.Poziom = 0;
                    session.Save();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Bledy konta proste {0}", NrKonta);
                    Console.WriteLine(e);
                }
            }

            if (counter == 1 && account == "205")
            {
                bool checkacc = Char.IsNumber(NrKonta, 4);
                string czlon1 = NrKonta.Substring(0, NrKonta.IndexOf("-"));
                string czlon2 = NrKonta.Substring(NrKonta.IndexOf("-") + 1, NrKonta.Length - NrKonta.IndexOf("-") - 1);

                if (checkacc == false)
                {

                    try
                    {

                        var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                        var konto = (CDNKH.IKonto)konta.AddNew(null);
                        var Kontrahenci = (CDNBase.ICollection)(session.CreateObject("CDN.Kontrahenci", null));
                        var kontrahent = (CDNHeal.IKontrahent)Kontrahenci[$"Knt_Kod='{czlon2}'"];

                        var rodzickonto = (CDNKH.IKonto)konta[$"Acc_NumerIdx ='{account}'"];

                        konto.Poziom = 2;
                        konto.Analityka = 1;
                        konto.SlownikId = kontrahent.GIDNumer;
                        konto.ParId = rodzickonto.ID;
                        konto.OkresId = rodzickonto.OkresId;
                        //    konto.Slownik = 1;
                        konto.Nazwa = kontrahent.Akronim;
                        konto.Segment = kontrahent.Akronim;
                        konto.SlownikTyp = 1;
                        session.Save();
                    }
                    catch (Exception e)
                    {
                        //205-01


                        Console.WriteLine(e);
                        //   Console.ReadKey();
                    }
                }
                else
                {
                    try
                    {

                        var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                        var konto = (CDNKH.IKonto)konta.AddNew(null);
                        //     var Kontrahenci = (CDNBase.ICollection)(session.CreateObject("CDN.Kontrahenci", null));
                        //   var kontrahent = (CDNHeal.IKontrahent)Kontrahenci[$"Knt_Kod='{czlon2}'"];

                        var rodzickonto = (CDNKH.IKonto)konta[$"Acc_NumerIdx ='{account}'"];

                        konto.Poziom = 2;
                        konto.Analityka = 1;
                        //   konto.SlownikId = kontrahent.GIDNumer;
                        konto.ParId = rodzickonto.ID;
                        konto.OkresId = rodzickonto.OkresId;
                        //    konto.Slownik = 1;
                        konto.Nazwa = nazwa;
                        konto.Segment = czlon2;
                        //konto.SlownikTyp = 1;
                        session.Save();

                    }
                    catch (Exception e)
                    {

                    }

                }
            }


            if (counter == 2 && account == "205")
            {
                string czlon4 = NrKonta.Substring(0, NrKonta.IndexOf("-"));
                string czlon2full = NrKonta.Substring(NrKonta.IndexOf("-") + 1, NrKonta.Length - NrKonta.IndexOf("-") - 1);
                string czlon5 = czlon2full.Substring(0, czlon2full.IndexOf("-"));
                string czlon3 = czlon2full.Substring(czlon2full.IndexOf("-") + 1, czlon2full.Length - czlon2full.IndexOf("-") - 1);
                string czlon6 = czlon2full.Substring(0, czlon2full.IndexOf("-"));
                string czlonacc = czlon4 + "-" + czlon6;
                try
                {



                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var konto = (CDNKH.IKonto)konta.AddNew(null);

                    // var rodzicKonto = (CDNKH.IKonto)konta[$"Acc_Numer ={ kontor} "];
                    var rodzickonto = (CDNKH.IKonto)konta[$"Acc_Numer ='{czlonacc}'"];

                    konto.NumerIdx = NrKonta;
                    konto.Nazwa = nazwa;
                    konto.ParId = rodzickonto.ID;
                    konto.Segment = czlon4;
                    konto.OkresId = 1;
                    konto.Poziom = 3;
                    session.Save();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Bledy 2 {0}", NrKonta);
                    Console.WriteLine(e);
                }
            }






            if (counter == 0 && account == "200" || account=="202")
            {
                try
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var konto = (CDNKH.IKonto)konta.AddNew(null);


                    //konto.NumerIdx = NrKonta;
                    konto.Nazwa = NrKonta;
                    konto.TypKonta = 1;
                    //konto.Poziom = 1;
                    konto.Analityka = 0;
                    //konto.NextAcc = NrKonta;
                    konto.Segment = NrKonta;
                    konto.OkresId = 1;
                    //  konto.Poziom = 0;
                    session.Save();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Bledy konta proste {0}", NrKonta);
                    Console.WriteLine(e);
                }
            }    
            if (counter == 1 && account == "200" || account == "202")
                {

                    string kont = NrKonta.Substring(NrKonta.IndexOf('-') + 1, NrKonta.Length - NrKonta.IndexOf('-') - 1);

                    try
                    {


                        //kont = NrKonta.Substring(NrKonta.IndexOf('-') + 1, NrKonta.Length - NrKonta.IndexOf('-') - 1);

                        var konta = (ICollection)session.CreateObject("CDN.Konta", null);
        var konto = (CDNKH.IKonto)konta.AddNew(null);
        var Kontrahenci = (CDNBase.ICollection)(session.CreateObject("CDN.Kontrahenci", null));
        var kontrahent = (CDNHeal.IKontrahent)Kontrahenci[$"Knt_Kod='{kont}'"];

        string testt = account;
        var rodzickonto = (CDNKH.IKonto)konta[$"Acc_NumerIdx ='{account}'"];

        konto.Poziom = 2;
                        konto.Analityka = 1;
                        konto.SlownikId = kontrahent.GIDNumer;
                        konto.ParId = rodzickonto.ID;
                        konto.OkresId = rodzickonto.OkresId;
                        //   konto.Slownik = 1;
                        konto.Nazwa = kontrahent.Akronim;
                        konto.Segment = kont;
                        konto.SlownikTyp = 1;
                        session.Save();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        Console.WriteLine("Bledy konta 200 {0}  {1}", NrKonta, kont);
                    }
                }

            





            }
        public void Wylogowanie()
        {
            // niszczymy Login
            Login = null;
            // odblokowanie (wylogowanie) O!
            Application.UnlockApp();
            // niszczymy obiekt Aplikacji
            Application = null;
        }

        public bool Add(string kod, string name, string ean, string color,
           decimal price, string desc, string size, string nazwazdjecia1, string nazwazdjecia2, string twrgrupa, int Qty, string prod)
        {
            try
            {
                CDNBase.AdoSession Sesja = Login.CreateSession();

                var towary = (CDNTwrb1.Towary)Sesja.CreateObject("CDN.Towary", null);

                var towar = (CDNTwrb1.ITowar)towary.AddNew(null);

                var grupy = (CDNBase.ICollection)(Sesja.CreateObject("CDN.TwrGrupy", null));
                var grupa = (CDNTwrb1.TwrGrupa)grupy["twg_kod ='" + Properties.Settings.Default.Default_twr_group + "'"];


                var pr = (CDNBase.ICollection)(Sesja.CreateObject("CDN.Producenci", null));
                try
                {
                    var pr1 = (OP_Twrb2Lib.Producent)pr["Prd_kod ='" + prod + "'"];
                    towar.PrdID = pr1.ID;
                }
                catch
                {
                    var pr1 = (OP_Twrb2Lib.Producent)pr.AddNew();
                    pr1.Kod = prod;
                    pr1.Nazwa = prod;
                    towar.PrdID = pr1.ID;
                    logger.Error("blad w producencie");

                }




                towar.Nazwa = name;
                towar.Kod = kod;
                towar.Stawka = 23.00m;
                towar.EAN = ean;
                towar.Flaga = 2;

                towar.Opis = desc;



                towar.TwGGIDNumer = grupa.GIDNumer;



                towar.JM = "SZT";
                towar.ESklep = 1;

                string linka1 = nazwazdjecia1;
                string linkname1 = "";
                for (int i = linka1.Length - 1; i > 0; i--)
                {
                    if (linka1[i] != '/')
                    {
                        linkname1 = linka1[i] + linkname1;

                    }
                    else
                    {
                        break;
                    }

                }
                var zdjecia = (DaneBinarne)Sesja.CreateObject("CDN.DaneBinarne");
                if (nazwazdjecia1.Length>0)
                {

                


                var zdj = (IDanaBinarna)zdjecia.AddNew();


                zdj.Nazwa = linkname1;
                zdj.NazwaPliku = linkname1;
                zdj.DodajPlik((@Properties.Settings.Default.Sciezka_do_zdjec + linkname1));
                Sesja.Save();
                var link = (DaneBinarneLink)towar.Zalaczniki.AddNew();
                link.DABID = zdj.ID;
                link.TwrID = towar.ID;
                link.Typ = 4;
                link.NazwaPliku = zdj.NazwaPliku;
                link.WBazie = 1;
                link.FileMode = 2;
                link.ESklep = 1;
                Sesja.Save();

                }
                string linka2 = nazwazdjecia2;
                string linkname2 = "";
                for (int i = linka2.Length - 1; i > 0; i--)
                {
                    if (linka2[i] != '/')
                    {
                        linkname2 = linka2[i] + linkname2;

                    }
                    else
                    {
                        break;
                    }

                }

                if (nazwazdjecia2.Length > 0)
                {
                    var zdjecia1 = (DaneBinarne)Sesja.CreateObject("CDN.DaneBinarne");
                    var zdj1 = (IDanaBinarna)zdjecia.AddNew();

                    zdj1.Nazwa = linkname2;
                    zdj1.NazwaPliku = linkname2;
                    zdj1.DodajPlik((@Properties.Settings.Default.Sciezka_do_zdjec + linkname2));
                    Sesja.Save();

                    var link1 = (DaneBinarneLink)towar.Zalaczniki.AddNew();
                    link1.DABID = zdj1.ID;
                    link1.TwrID = towar.ID;
                    link1.Typ = 4;
                    link1.NazwaPliku = zdj1.NazwaPliku;
                    link1.WBazie = 1;
                    link1.FileMode = 2;
                    link1.ESklep = 1;
                    Sesja.Save();

                }

                var ekslep = (ITwrESklep)towar.ParametryESklep.AddNew();
                ekslep.ESklepId = Properties.Settings.Default.Esklep_ID;
     //           ekslep.TwGGIDNumer = grupa.GIDNumer;
                ekslep.Udostepnij = 0;
                ekslep.ESklepDostepnosc = 4;
                var jez = (ITwrJezykObcy)towar.NazwyJezykiObce.AddNew();
                jez.JezykId = 1;


                jez.ESklepId = Properties.Settings.Default.Esklep_ID;
                jez.OpisHTML = desc;
                jez.NazwaHTML = name; 

                var atrybut1 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                //35
                atrybut1.DeAID = Properties.Settings.Default.ATR_COLOR_ID;

                atrybut1.Wartosc = color;

                atrybut1.ESklep = 1;
                atrybut1.AtrybutGrupujacyFantom = 1;
                Sesja.Save();


                var atrybut2 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                atrybut2.DeAID = Properties.Settings.Default.ATR_SIZE_ID;
                //34
                atrybut2.Wartosc = size;
                atrybut2.ESklep = 1;
                atrybut2.AtrybutGrupujacyFantom = 1;
                Sesja.Save();

                var atrybut3 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                atrybut3.DeAID = Properties.Settings.Default.ATR_XML_ID;
                //43
                atrybut3.Wartosc = "TAK";
                Sesja.Save();

                foreach (ICena cena in towar.Ceny)
                {
                    if (cena.Numer == 1)
                    {
                        cena.WartoscNetto = price;
                    }
                    else if (cena.Numer == 2)
                    {
                        cena.Wartosc = price;
                    }
                }



                Sesja.Save();
                return true;
            }
            






            catch (Exception e)

            {
                logger.Error(e);
                return false;
           //     Console.Writeline(e);
               
                logger.Debug("----------------------------------------------------------------------");
                //Console.WriteLine(e);
                logger.Error(e);
                //GetSql sql = new GetSql();
                //var ids = sql.CheckTwrID(kod);
                //foreach (var id in ids)
                //{
                //  sql.UpdateQty(id.TWRXML_ID, Qty);
                // }
                //logger.Error("Towar o kodzie {0} juz istnieje. Aktualizuje ilosc na:{1}", kod, Qty);



            }






        }

        public bool AddXML2(string kod, string name, string ean, string color,
           decimal price, string desc, string size, string nazwazdjecia1, string nazwazdjecia2, string twrgrupa, int Qty, string prod)
        {

            int gidnumer;


            try
            {
                CDNBase.AdoSession Sesja = Login.CreateSession();

                var towary = (CDNTwrb1.Towary)Sesja.CreateObject("CDN.Towary", null);

                var towar = (CDNTwrb1.ITowar)towary.AddNew(null);


                var grupy = (CDNBase.ICollection)(Sesja.CreateObject("CDN.TwrGrupy", null));
          
                      var grupa = (CDNTwrb1.TwrGrupa)grupy["twg_kod ='" + Properties.Settings.Default.Default_twr_group + "'"];

                   


           

                towar.Nazwa = name;
                towar.Kod = kod;
                towar.Stawka = 23.00m;
                towar.EAN = ean;
                towar.Flaga = 2;
                //     towar.KodDostawcy = brand;
                towar.Opis = desc;
                //  towar.TwGGIDNumer = ;

                towar.TwGGIDNumer = grupa.GIDNumer;







                towar.JM = "SZT";
                towar.ESklep = 1;
                //towar.PrdID = pr1.ID;

                var zdjecia = (DaneBinarne)Sesja.CreateObject("CDN.DaneBinarne");
                check files = new check();
                var files1 = files.filexist(nazwazdjecia1);
                if (files1 == true)
                {


                    var zdj = (IDanaBinarna)zdjecia.AddNew();

                    string sizefile = Properties.Settings.Default.Sciezka_do_zdjec + nazwazdjecia1;
                    FileInfo fi1 = new FileInfo(sizefile);
                    zdj.Nazwa = nazwazdjecia1;
                    zdj.NazwaPliku = nazwazdjecia1;
                    zdj.DodajPlik(@Properties.Settings.Default.Sciezka_do_zdjec + nazwazdjecia1);
                    Sesja.Save();
                    var link = (DaneBinarneLink)towar.Zalaczniki.AddNew();
                    link.DABID = zdj.ID;
                    link.TwrID = towar.ID;
                    link.Typ = 4;
                    link.NazwaPliku = zdj.NazwaPliku;
                    link.WBazie = 1;
                    link.FileMode = 2;
                    if (fi1.Length > 500000)
                    {

                    }
                    else
                    {
                        link.ESklep = 1;
                    }
                    //   link.ESklep = 1;
                    Sesja.Save();
                }
                else
                {
                    logger.Error("Nie mam takiego zdjecia pomijam dodawanie");
                }
                check fil2es2 = new check();


                var file2 = files.filexist(nazwazdjecia2);
                if (file2 == true)
                {
                    var zdjecia1 = (DaneBinarne)Sesja.CreateObject("CDN.DaneBinarne");
                    var zdj1 = (IDanaBinarna)zdjecia.AddNew();
                    string sizefile1 = @Properties.Settings.Default.Sciezka_do_zdjec + nazwazdjecia2;
                    
                    FileInfo fi2 = new FileInfo(sizefile1);
                  
                    
                    zdj1.Nazwa = nazwazdjecia2;
                    zdj1.NazwaPliku = nazwazdjecia2;
                    zdj1.DodajPlik(@Properties.Settings.Default.Sciezka_do_zdjec + nazwazdjecia2);
                    Sesja.Save();

                    var link1 = (DaneBinarneLink)towar.Zalaczniki.AddNew();
                    link1.DABID = zdj1.ID;
                    link1.TwrID = towar.ID;
                    link1.Typ = 4;
                    link1.NazwaPliku = zdj1.NazwaPliku;
                    link1.WBazie = 1;
                    link1.FileMode = 2;
                    if (fi2.Length > 500000)
                    {

                    }
                    else
                    {
                        link1.ESklep = 1;
                    }
                    //     link1.ESklep = 1;
                    Sesja.Save();

                }
                else
                {
                    logger.Error("Nie mam takiego zdjecia pomijam dodawanie");


                }
                var ekslep = (ITwrESklep)towar.ParametryESklep.AddNew();
                ekslep.ESklepId = Properties.Settings.Default.Esklep_ID;
              
                

                ekslep.Udostepnij = 1;
                ekslep.ESklepDostepnosc = 4;
                var jez = (ITwrJezykObcy)towar.NazwyJezykiObce.AddNew();
                jez.JezykId = 1;


                jez.ESklepId = Properties.Settings.Default.Esklep_ID;
                jez.OpisHTML = desc;
                jez.NazwaHTML = name;

                var atrybut1 = (ITwrAtrybut)towar.Atrybuty.AddNew();

                atrybut1.DeAID = Properties.Settings.Default.ATR_COLOR_ID;

                atrybut1.Wartosc = color;

                atrybut1.ESklep = 1;
                atrybut1.AtrybutGrupujacyFantom = 1;
                Sesja.Save();


                var atrybut2 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                atrybut2.DeAID = Properties.Settings.Default.ATR_SIZE_ID;
                atrybut2.Wartosc = size;
                atrybut2.ESklep = 1;
                atrybut2.AtrybutGrupujacyFantom = 1;
                Sesja.Save();

                var atrybut3 = (ITwrAtrybut)towar.Atrybuty.AddNew();
                atrybut3.DeAID = Properties.Settings.Default.ATR_XML_ID;
                atrybut3.Wartosc = "TAK";
                Sesja.Save();

                foreach (ICena cena in towar.Ceny)
                {
                    if (cena.Numer == 1)
                    {
                        cena.WartoscNetto = price;
                    }
                    else if (cena.Numer == 2)
                    {
                        cena.Wartosc = price;
                    }
                }



                Sesja.Save();

                return true;




            }
            catch (Exception e)

            {
                logger.Error(e);
                logger.Error("----------------------------------------------------------------------");
                return false;
            }



        }
    }
}
  
    
