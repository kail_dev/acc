﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nattalex.DAL
{
   public class converter
    {
        public string NameFromLink(string linkname)
        {
            string link1 = linkname;
            string linkname1 = "";

            for (int i = link1.Length - 1; i > 0; i--)
            {
                if (link1[i] != '/')
                {
                    linkname1 = link1[i] + linkname1;

                }
                else
                {
                    break;
                }
                

            }
            return linkname1;
        }



        public string OneGroup (string grups)

        {
            string ngrup = grups;
            string ngr = "";
            for (int i = ngrup.Length - 1; i > 0; i--)
            {
                if (ngrup[i] != ',')
                {
                    ngr = ngrup[i] + ngr;

                }
                else
                {
                    break;
                }

            
            }
            string grupa = ngr.ToLower();
            return grupa;


        }


        public decimal PricetoIntPLN(string pricestring)
        {
            pricestring = pricestring.Remove(pricestring.Length - 4, 4);

            decimal price = decimal.Parse(pricestring.Replace(".", ","));
            return price;
        }

        public decimal PricetoIntWithoutPLN(string pricestring)
        {
          //  pricestring = pricestring.Remove(pricestring.Length - 4, 4);

            decimal price = decimal.Parse(pricestring.Replace(".", ","));
            return price;
        }

        public int QtyToInt(string qtystring)
        {

            decimal resultTmp;
            bool status = Decimal.TryParse(qtystring.Replace(".", ","), out resultTmp);
            int QtyInt = (int)resultTmp;
            return QtyInt;
        }

    }

}
