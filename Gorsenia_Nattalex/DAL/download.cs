﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;


namespace Nattalex.DAL
{
  public   class download
    {
       
        public  bool PobierzXML()
        {

            if (Properties.Settings.Default.Autoryzacja_https == 1)
            {


                try
                {
                    WebClient downloadWC = new WebClient();
                    //"http://xml.gorsenia.pl/xml?id=30"
                    downloadWC.Credentials = new System.Net.NetworkCredential(Properties.Settings.Default.Http_User, Properties.Settings.Default.Http_password);
                //    downloadWC.Credentials = new System.Net.NetworkCredential(Properties.Settings.Default.Http_User, Properties.Settings.Default.Http_password);

                    byte[] buff = downloadWC.DownloadData(Properties.Settings.Default.Link_do_XML);

                  

                    using (System.IO.FileStream fs = new FileStream(@Properties.Settings.Default.Sciezka_do_XML + Properties.Settings.Default.Nazwa_pliku_XML, FileMode.Create))
                    {
                        fs.Write(buff, 0, buff.Length);
                        fs.Close();
                        return true;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                   return false;
                }
            }
            else
            {
                try
                {
                    WebClient downloadWC = new WebClient();
                  

                    byte[] buff = downloadWC.DownloadData(Properties.Settings.Default.Link_do_XML);
               


                    using (System.IO.FileStream fs = new FileStream(@Properties.Settings.Default.Sciezka_do_XML + Properties.Settings.Default.Nazwa_pliku_XML, FileMode.Create))
                    {
                        fs.Write(buff, 0, buff.Length);
                        fs.Close();
                        return true;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                    return false;
                }

            }
        }


        public bool PobierzIMG(string sciezka, string nazwapliku)
        {
            if (sciezka.Length > 0)
            {
                try
                {

                    using (WebClient webClient = new WebClient())
                    {
                        webClient.DownloadFile(sciezka, Properties.Settings.Default.Sciezka_do_zdjec + nazwapliku);

                    }

                    return true;
                }
                catch (WebException e)
                {
                    Console.WriteLine(e);
                    return false;
                    //  Console.ReadKey();
                }
            }
            else
            {
                return false;
            }
        }


    }
}
