﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nattalex.DAL
{
   public class sqlmetods
    {
        public virtual bool Insert(sqlentity e)
        {
            Entities ent = new Entities();

            var plcRecord = ent.Set<ZZSPE_TWRXML_V2>();

            plcRecord.Add(new ZZSPE_TWRXML_V2() { TWRXML_KOD =e.TWRXML_KOD,TWRXML_TWR_EAN = e.TWRXML_TWR_EAN, TWRXML_ILOSC = e.TWRXML_ILOSC });


            return ent.SaveChanges() > 0;

        }


        public virtual bool UpdateQty(int id, int ilosc)
        {
            Entities ent = new Entities();
            ZZSPE_TWRXML_V2 plcRecord = ent.ZZSPE_TWRXML_V2.Where(e => e.TWRXML_ID == id).First();
            plcRecord.TWRXML_ILOSC = ilosc;

            return ent.SaveChanges() > 0;


        }


        public virtual List<sqlentity> CheckTwrID(string twr_kod)

        {
            Entities ent = new Entities();
            List<sqlentity> outcome = new List<sqlentity>();
            List<ZZSPE_TWRXML_V2> result = ent.ZZSPE_TWRXML_V2.Where(e => e.TWRXML_KOD == twr_kod).ToList();
            foreach (ZZSPE_TWRXML_V2 item in result)
            {
                sqlentity sqte = new sqlentity();
                sqte.TWRXML_ID = item.TWRXML_ID;
                sqte.TWRXML_TWR_EAN = item.TWRXML_TWR_EAN;
                sqte.TWRXML_ILOSC = item.TWRXML_ILOSC.Value;

                outcome.Add(sqte);

            }

            return outcome;




        }
    }
}
